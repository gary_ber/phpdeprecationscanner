<?php

namespace phpDeprecationScanner\extras;

/**
 * Interface ExtrasInterface
 * @package phpDeprecationScanner\extras
 */

interface ExtrasInterface
{

    public function check($tokens);

}
