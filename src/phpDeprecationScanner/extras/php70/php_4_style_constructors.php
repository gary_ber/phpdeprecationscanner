<?php

namespace phpDeprecationScanner\Extras\PHP70;

use phpDeprecationScanner\extras\ExtrasInterface;

/**
 * PHP 4 style constructors
 * http://php.net/manual/en/migration70.deprecated.php
 * Class php_4_style_constructors
 * @package phpDeprecationScanner\Extras\PHP70
 */

class php_4_style_constructors implements ExtrasInterface
{

    public function check($tokens)
    {

    }

}
