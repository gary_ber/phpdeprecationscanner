<?php

namespace phpDeprecationScanner\Extras\PHP70;

use phpDeprecationScanner\extras\ExtrasInterface;

/**
 * Static calls to non-static methods
 * http://php.net/manual/en/migration70.deprecated.php
 * Class static_calls_to_non_static_methods
 * @package phpDeprecationScanner\Extras\PHP70
 */

class static_calls_to_non_static_methods implements ExtrasInterface
{
    public function check($tokens)
    {

    }
}
