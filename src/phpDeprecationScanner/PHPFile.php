<?php

namespace phpDeprecationScanner;

/**
 * Class phpFile
 * @package phpDeprecationScanner
 */

class PHPFile
{

    protected $fileLocation = '';

    /**
     * @var Deprecator $deprecator
     */
    protected $deprecator;

    /**
     * @var Log $log
     */
    protected $log;

    protected $deprecatedFunctions = [];

    /**
     * PHPFile constructor.
     * @param String $fileLocation
     * @param Deprecator $deprecator
     * @throws \Exception
     */
    public function __construct(String $fileLocation, Deprecator &$deprecator, Log &$log)
    {
        if (!is_file($fileLocation)){
            throw new \Exception('PHP File not found.');
        }
        $this->fileLocation = $fileLocation;
        $this->deprecator = $deprecator;
        $this->log = $log;
    }

    /**
     * @return array
     */
    public function getDeprecatedFunctions()
    {
        return $this->deprecatedFunctions;
    }

    /**
     * @return mixed
     */
    public function runCheck()
    {
        $PHPFileContents = file_get_contents($this->getFileLocation());
        if ($PHPFileContents) {
            $lineNumber = 0;
            $tokensArray = token_get_all($PHPFileContents);
            $functionDefining = false;
            $lastStringToken = null;
            foreach($tokensArray as $token){
                if(is_array($token)){
                    $tokenNamed = array(
                        'name' => token_name($token[0]),
                        'value' => $token[1],
                        'line' => $token[2]
                    );
                    $lineNumber = $tokenNamed['line'];
                }else{
                    $tokenNamed = array(
                        'name' => 'T_NA',
                        'value' => $token,
                        'line' => $lineNumber
                    );
                }
                // TODO check for Static class methods
                // TODO check if function is re-defined somewhere in code (will require to scan and create a list of defined functions)
                // TODO check if function is re-defined in a class (will require to scan and create a list of defined functions)
                switch($tokenNamed['name'])
                {
                    case 'T_FUNCTION':
                        $functionDefining = true;
                        break;
                    case 'T_STRING':
                        if($functionDefining === false){
                            $lastStringToken = $tokenNamed;
                        }
                        break;
                    case 'T_NA':
                        switch($tokenNamed['value'])
                        {
                            case '(':
                                $functionDefining = false;
                                if(is_array($lastStringToken))
                                {
                                    $deprecatedFunction = $this->deprecator->isFunctionNameDeprecated($lastStringToken['value']);
                                    if($deprecatedFunction !== false)
                                    {
                                        $this->log->deprecatedPHPFunctionFound($this, $lastStringToken['value'], $lastStringToken['line']);
                                        array_push($this->deprecatedFunctions, $deprecatedFunction);
                                    }
                                    $lastStringToken = null;
                                }
                                break;
                            case ')':
                                break;
                        }
                        break;
                    case 'T_WHITESPACE':
                        break;
                    default:
                        $functionDefining = false;
                        break;
                }
            }
            return $this->deprecatedFunctions;
        }
        $this->log->emptyPHPFile($this);
        return false;
    }

    /**
     * @return string
     */
    public function getFileLocation()
    {
        return $this->fileLocation;
    }

}
