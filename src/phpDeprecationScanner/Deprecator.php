<?php

namespace phpDeprecationScanner;

/**
 * Class Deprecator
 * @package phpDeprecationScanner
 */

class Deprecator
{

    /**
     * @var object $deprecations
     */
    protected $deprecations;

    protected $deprecatedFunctionNameCache = [];

    /**
     * Deprecator constructor.
     * @param String $deprecationsFile
     * @throws \Exception
     */
    public function __construct(String $deprecationsFile)
    {
        $deprecationsContent = file_get_contents($deprecationsFile);
        $deprecationsArray = json_decode($deprecationsContent, true);
        if($deprecationsContent && isset($deprecationsArray['php_releases'])){
            $deprecationsArray = $deprecationsArray['php_releases'];
        } else {
            throw new \Exception('Deprecations php_releases not found');
        }
        $this->deprecations = $this->generateDeprecationsObject($deprecationsArray);
        $this->deprecatedFunctionNameCache = $this->generateDeprecatedFunctionNameList($this->deprecations);
    }

    /**
     * @param \stdClass $deprecations
     * @return array
     */
    private function generateDeprecatedFunctionNameList(\stdClass $deprecations)
    {
        $deprecatedFunctionNameList = [];
        foreach($deprecations->phpReleases as $phpRelease)
        {
            if(
                (isset($phpRelease->data)) &&
                (isset($phpRelease->data->php)) &&
                (isset($phpRelease->data->php->deprecated)) &&
                (isset($phpRelease->data->php->deprecated->functions))
            )
            {
                foreach($phpRelease->data->php->deprecated->functions as $function)
                {
                    $deprecatedFunctionNameList[$function->cleanName] = $function;
                }
            }
        }
        return $deprecatedFunctionNameList;
    }

    /**
     * @param array $deprecationsArray
     * @return object
     */
    private function generateDeprecationsObject(Array $deprecationsArray)
    {
        $deprecations = new \stdClass();
        $deprecations->phpReleases = [];
        foreach($deprecationsArray as $phpRelease)
        {
            $phpReleaseObject = new \stdClass();
            $phpReleaseObject->version = isset($phpRelease['php_version']) ? $phpRelease['php_version'] : '';
            $phpReleaseObject->doc = isset($phpRelease['php_doc']) ? $phpRelease['php_doc'] : '';
            $phpReleaseObject->note = isset($phpRelease['note']) ? $phpRelease['note'] : '';
            if(isset($phpRelease['data']))
            {
                $data = $phpRelease['data'];
                $phpReleaseObject->data = new \stdClass();
                if(isset($data['php']))
                {
                    $php = $data['php'];
                    $phpReleaseObject->data->php = new \stdClass();
                    if(isset($php['deprecated']))
                    {
                        $deprecated = $php['deprecated'];
                        $phpReleaseObject->data->php->deprecated = new \stdClass();
                        if(isset($deprecated['functions']))
                        {
                            $functions = $deprecated['functions'];
                            $phpReleaseObject->data->php->deprecated->functions = [];
                            foreach($functions as $function)
                            {
                                $functionObject = new class($phpReleaseObject) {

                                    public $name = '';

                                    public $cleanName = '';

                                    public $note = '';

                                    private $phpReleaseObject = null;

                                    public function __construct(&$phpReleaseObject)
                                    {
                                        $this->phpReleaseObject = $phpReleaseObject;
                                    }

                                    /**
                                     * @return mixed
                                     */
                                    public function getPHPRelease()
                                    {
                                        return $this->phpReleaseObject;
                                    }

                                };
                                $functionObject->name = isset($function['name']) ? $function['name'] : '';
                                $functionObject->cleanName = strtolower(str_replace(['(',')'], '', trim($functionObject->name)));
                                $functionObject->note = isset($function['note']) ? $function['note'] : '';
                                array_push($phpReleaseObject->data->php->deprecated->functions, $functionObject);
                            }
                        }
                    }
                }
            }
            array_push($deprecations->phpReleases, $phpReleaseObject);
        }
        return $deprecations;
    }

    /**
     * @return object
     */
    public function getDeprecations()
    {
        return $this->deprecations;
    }

    /**
     * @param String $functionName
     * @return bool
     */
    public function isFunctionNameDeprecated(String $functionName)
    {
        $functionName = strtolower($functionName);
        if(isset($this->deprecatedFunctionNameCache[$functionName]))
        {
            return $this->deprecatedFunctionNameCache[$functionName];
        }
        return false;
    }

}