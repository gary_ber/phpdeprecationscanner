<?php

namespace phpDeprecationScanner;

/**
 * Class Log
 * @package phpDeprecationScanner
 */

class Log
{

    protected $logToFile = false;

    protected $echoOutput = false;

    protected $logFile = '';

    protected $deprecatedFunctionCount = 0;

    /**
     * phpDeprecationScannerOutput constructor.
     * @param String $outputLogFile
     * @param bool $echoOutput
     */
    public function __construct(String $outputLogFile = '', $echoOutput = false)
    {
        if($outputLogFile)
        {
            $this->logFile = $outputLogFile;
            $this->logToFile = true;
            if (!is_file($this->logFile))
            {
                file_put_contents($this->logFile, '');
            }
        }
        $date = date('m/d/Y h:i:s a', time());
        $this->echoOutput = $echoOutput;
        $this->insertLine("\n");
        $this->insert("\nNew scan: $date\n");
    }

    /**
     * @param PHPFile $PHPFile
     */
    public function foundPHPFile(PHPFile $PHPFile)
    {
        $message = "Found PHP file: {$PHPFile->getFileLocation()}\n";
        $this->insertLine('', "\n");
        $this->insert($message);
    }

    /**
     * @param PHPFile $PHPFile
     */
    public function phpFileHasDeprecations(PHPFile $PHPFile)
    {
        $deprecatedFunctionCount = count($PHPFile->getDeprecatedFunctions());
        $message = "PHP file has Deprecations: {$PHPFile->getFileLocation()}\nDeprecated Function Count: $deprecatedFunctionCount";
        $this->insert($message);
    }

    /**
     * @param PHPFile $PHPFile
     */
    public function emptyPHPFile(PHPFile $PHPFile)
    {
        $message = "PHP File is empty: {$PHPFile->getFileLocation()}";
        $this->insert($message);
    }

    /**
     * @param PHPFile $PHPFile
     */
    public function invalidPHPFile(PHPFile $PHPFile)
    {
        $message = "Invalid PHP file: {$PHPFile->getFileLocation()}";
        $this->insert($message);
    }

    /**
     * @param PHPFile $PHPFile
     * @param String $functionName
     * @param Int $lineNumber
     */
    public function deprecatedPHPFunctionFound(PHPFile $PHPFile, String $functionName, Int $lineNumber)
    {
        $this->deprecatedFunctionCount++;
        $message = "Deprecated Function Found: $functionName() on {$PHPFile->getFileLocation()} Line# $lineNumber";
        $this->insert($message);
    }

    /**
     * @param String $prefix
     * @param String $suffix
     */
    public function insertLine(String $prefix = '', String $suffix = '')
    {
        $this->insert($prefix . str_repeat('-', 10) . $suffix);
    }

    /**
     * Write to log
     *
     * @param String $message
     */
    public function insert(String $message)
    {
        $message .= "\n";
        if ($this->echoOutput)
        {
            echo $message;
        }
        if ($this->logToFile)
        {
            file_put_contents($this->logFile, $message, FILE_APPEND);
        }
    }

    /**
     * @return int
     */
    public function getDeprecatedFunctionCount()
    {
        return $this->deprecatedFunctionCount;
    }

}
