#phpDeprecationScanner

###Find PHP deprecated language tokens and functions

--

####Run CLI:

--help: For help

```bash
php run.php --help
```


-dir: Directory

--output: Output log file

--echo: Echo output to shell (Default: true)

--ignore: Ignore directory (Comma separated)

```bash
php run.php --dir ./test --output="./output/php_test.log" --echo="true" --ignore="./test/recursive/php5.3_ignore.php"
```
