<?php

// Deprecated Functions

ldap_sort();

// Deprecated - PHP 4 style constructors

class foo1 {
    function foo1() {
        echo 'I am the constructor';
    }
}

// Deprecated - Static calls to non-static methods

class foo2 {
    function bar() {
        echo 'I am not static!';
    }
}

foo2::bar();
