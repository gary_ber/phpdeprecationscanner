<?php

/**
 * phpDeprecationScanner CLI
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$loader = require 'vendor/autoload.php';

$timeStart = microtime(true);

$echoOutput = true;

$options = getopt('',[
    'help::',
    'dir:',
    'output::',
    'echo::',
    'ignore::'
]);

if(isset($options['help']))
{
    echo "phpDeprecationScanner\n";
    echo "https://bitbucket.org/gary_ber/phpdeprecationscanner/src/master/\n\n";
    echo "List of CLI parameters:\n\n";
    echo "\t--help For help\n";
    echo "\t-dir Directory to scan.\n";
    echo "\t--output Output log file.\n";
    echo "\t--echo Echo output to shell (Default: true)\n";
    echo "\t--ignore Ignore directory (Comma separated)\n";
    echo "\n";
    die();
}

echo "Starting phpDeprecationScanner...\n";

$outputLogFile = isset($options['output']) ? $options['output'] : '';
$scanDirectory = isset($options['dir']) ? $options['dir'] : '';

if(isset($options['echo']) && $options['echo'] === 'false')
{
    $echoOutput = false;
}

$ignoreDirectoryList = [];

if(isset($options['ignore']))
{
    $ignoreDirectoryList = explode(',', $options['ignore']);
}

$deprecationsFile = './deprecations.json';

try {
    $phpDeprecationScanner = new phpDeprecationScanner\Scanner($deprecationsFile, $outputLogFile, $echoOutput);
    $log = $phpDeprecationScanner->scan($scanDirectory, $ignoreDirectoryList);
    echo "\nDeprecated Functions Found: {$log->getDeprecatedFunctionCount()}\n";
}
catch(Exception $e){
    echo "\nphpDeprecationScanner Error: {$e->getMessage()}\n\n";
}

$timeEnd = microtime(true);
$duration = $timeStart - $timeEnd;
$hours = (int)($duration / 60 / 60);
$minutes = (int)($duration / 60 ) - $hours * 60;
$seconds = abs((int)$duration - $hours * 60 * 60 - $minutes * 60);
echo "Total Execution Time: $hours Hours $minutes Minutes $seconds Seconds\n";
echo "Done\n\n";
